from django.db import models

# Create your models here.
class Cuenta(models.Model):
	Opcion1 = (      
              ('1', 'BANCOESTADO'),
              ('2', 'COPEUCH'),
              ('3', 'BANCO BBVA'),
              ('4', 'BANCO CONSORCIO'),
              ('5', 'BANCO RIPLEY'),
              ('6', 'BANCO FALABELLA'),
              ('7', 'THE BANK OF TOKYO-MITSUBISHI UFJ'),
              ('8', 'BANCO ITAU'),
              ('9', 'BANCO SANTANDER'),
              ('10', 'BICE'),
              ('11', 'CORPBANCA'),
              ('12', 'BANCO DE CREDITO E INVERSIONES'),
              ('13', 'SCOTIABANK CHILE'),
              ('14', 'BANCO INTERNACIONAL'),
              ('15', 'BANCO DE CHILE '),

       	)      
      
      
    
	nombre = models.CharField(max_length=144)
	apellido = models.CharField(max_length=144)
	creacion = models.DateTimeField(auto_now_add=True)
	correo= models.EmailField(max_length=254)
	password = models.CharField(max_length=128)
	Numero_cuenta = models.CharField(max_length=128)
	RUT = models.CharField(max_length=128)
	Banco=models.CharField(max_length=144 , choices=Opcion1)
	Opcion2 = (
      
              ('1', 'Cuenta corriente'),
              ('2', 'Chequera electronica'),
              ('3', 'Cuenta ahorro'),
              ('4', 'Cuenta rut '),
              ('5', 'Cuenta vista'),

   		 )
	Tipo_Cuenta=models.CharField(max_length=144 , choices=Opcion2)
	
	def __str__(self):
		return self.nombre